package ma.cimr.contrat.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class CaptchaFilter  extends OncePerRequestFilter {
    public static final String CAPTCHA = "captcha";
    @Autowired
    RestTemplate restTemplate;
    @Value("${google.captcha.url}")
    private String captchaUrl;
    @Value("${google.captcha.secret}")
    private String secret;



    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        HttpEntity<String> entity = new HttpEntity<String>("",headers);
//        final String response = httpServletRequest.getHeader(CAPTCHA);
//        if(isValid(response))
//            filterChain.doFilter(httpServletRequest, httpServletResponse);
//        else
//            logger.warn("JWT Token does not begin with Bearer String");
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

  /*  public boolean isValid(String clientRecaptchaResponse) throws IOException, ParseException {
        if (clientRecaptchaResponse == null || "".equals(clientRecaptchaResponse)) {
            return false;
        }

        URL obj = new URL(captchaUrl);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        //add client result as post parameter
        String postParams =
                "secret=" + secret +
                        "&response=" + clientRecaptchaResponse;

        // send post request to google recaptcha server
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        System.out.println("Post parameters: " + postParams);
        System.out.println("Response Code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());

        //Parse JSON-response
        JSONParser parser = new JSONParser(response.toString(),null,true);
        JSONObject json = (JSONObject) parser.parse();


        Boolean success = (Boolean) json.get("success");
        Double score = (Double) json.get("score");

        System.out.println("success : " + success);
        System.out.println("score : " + score);

        //result should be sucessfull and spam score above 0.5
        return (success && score >= 0.5);
    }*/


}
