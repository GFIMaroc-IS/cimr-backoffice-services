package ma.cimr.contrat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DELEGATAIRE")
public class Delegataire implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4696153144194283179L;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "delegat_seq")
	@SequenceGenerator(name = "delegat_seq", sequenceName = "delegat_seq", allocationSize=1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="ID_CONTRAT")
	private Contrat contrat;
	
	@Column(name="NOM")
	private String nom;
	
	@Column(name="PRENOM")
	private String prenom;
	
	@Column(name="CIN")
	private String cin;
	
	@Column(name="FONCTION")
	private String fonction;
	
	@ManyToOne
	@JoinColumn(name="ID_CATEGORIE")
	private RefCategorieHabilitation categorieHabilitation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public RefCategorieHabilitation getCategorieHabilitation() {
		return categorieHabilitation;
	}

	public void setCategorieHabilitation(RefCategorieHabilitation categorieHabilitation) {
		this.categorieHabilitation = categorieHabilitation;
	}
	
}
