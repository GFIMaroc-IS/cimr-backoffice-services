package ma.cimr.contrat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="REF_MODE_PAIEMENT")
public class RefModePaiement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3290646369728001033L;

	@Id
	@Column(name="ID_MODE_PAIEMENT")
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="CODE")
	private String code;
	
	@Column(name="LIBELLE")
	private String libelle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
