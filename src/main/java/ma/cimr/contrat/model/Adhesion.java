package ma.cimr.contrat.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="ADHESION")
public class Adhesion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6204880060525978338L;

	@Id
	@Column(name="ID_ADHESION")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NUM_RC")
	private String numRC;
	
	@Column(name="RAISON_SOC")
	private String raisonSociale;
	
	@Column(name="ADRESSE")
	private String adresse;
	
	@Column(name="FORME_JUR")
	private String formeJuridique;
	
	@Column(name="ICE")
	private String ice;
	
	@Column(name="IFU")
	private String ifu;
	
	@Column(name="CNSS")
	private String cnss;
	
	@Column(name="NUM_TAXE")
	private String numTaxe;
	
	@Column(name="NOM_MAND")
	private String nomMandataire;
	
	@Column(name="PRENOM_MAND")
	private String prenomMandataire;
	
	@Column(name="CIN_MAND")
	private String cinMandataire;
	
	@Column(name="FONCT_MAND")
	private String fonctMandataire;
	
	@Column(name="TEL_MAND")
	private String telMandataire;
	
	@Column(name="NOM_GEST")
	private String nomGestionnaire;
	
	@Column(name="PRENOM_GEST")
	private String prenomGestionnaire;
	
	@Column(name="EMAIL_GEST")
	private String emailGestionnaire;
	
	@Column(name="FONCT_GEST")
	private String fonctGestionnaire;
	
	@Column(name="TEL_GEST")
	private String telGestionnaire;
	
	@Column(name="RC_FILE")
	private Byte[] rcFile;
	
	@Column(name="ICE_FILE")
	private Byte[] iceFile;
	
	@OneToOne(mappedBy="adhesion",cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Contrat contrat;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNumRC() {
		return numRC;
	}

	public void setNumRC(String numRC) {
		this.numRC = numRC;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getFormeJuridique() {
		return formeJuridique;
	}

	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}

	public String getIce() {
		return ice;
	}

	public void setIce(String ice) {
		this.ice = ice;
	}

	public String getIfu() {
		return ifu;
	}

	public void setIfu(String ifu) {
		this.ifu = ifu;
	}

	public String getCnss() {
		return cnss;
	}

	public void setCnss(String cnss) {
		this.cnss = cnss;
	}

	public String getNumTaxe() {
		return numTaxe;
	}

	public void setNumTaxe(String numTaxe) {
		this.numTaxe = numTaxe;
	}

	public String getNomMandataire() {
		return nomMandataire;
	}

	public void setNomMandataire(String nomMandataire) {
		this.nomMandataire = nomMandataire;
	}

	public String getPrenomMandataire() {
		return prenomMandataire;
	}

	public void setPrenomMandataire(String prenomMandataire) {
		this.prenomMandataire = prenomMandataire;
	}

	public String getCinMandataire() {
		return cinMandataire;
	}

	public void setCinMandataire(String cinMandataire) {
		this.cinMandataire = cinMandataire;
	}

	public String getFonctMandataire() {
		return fonctMandataire;
	}

	public void setFonctMandataire(String fonctMandataire) {
		this.fonctMandataire = fonctMandataire;
	}

	public String getTelMandataire() {
		return telMandataire;
	}

	public void setTelMandataire(String telMandataire) {
		this.telMandataire = telMandataire;
	}

	public String getNomGestionnaire() {
		return nomGestionnaire;
	}

	public void setNomGestionnaire(String nomGestionnaire) {
		this.nomGestionnaire = nomGestionnaire;
	}

	public String getPrenomGestionnaire() {
		return prenomGestionnaire;
	}

	public void setPrenomGestionnaire(String prenomGestionnaire) {
		this.prenomGestionnaire = prenomGestionnaire;
	}

	public String getEmailGestionnaire() {
		return emailGestionnaire;
	}

	public void setEmailGestionnaire(String emailGestionnaire) {
		this.emailGestionnaire = emailGestionnaire;
	}

	public String getFonctGestionnaire() {
		return fonctGestionnaire;
	}

	public void setFonctGestionnaire(String fonctGestionnaire) {
		this.fonctGestionnaire = fonctGestionnaire;
	}

	public String getTelGestionnaire() {
		return telGestionnaire;
	}

	public void setTelGestionnaire(String telGestionnaire) {
		this.telGestionnaire = telGestionnaire;
	}

	public Byte[] getRcFile() {
		return rcFile;
	}

	public void setRcFile(Byte[] rcFile) {
		this.rcFile = rcFile;
	}

	public Byte[] getIceFile() {
		return iceFile;
	}

	public void setIceFile(Byte[] iceFile) {
		this.iceFile = iceFile;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	@Override
	public String toString() {
		String numRC = this.numRC != null ? "numRC : " + this.numRC + ", " : "";
		String raisonSociale = this.raisonSociale != null ? "raisonSociale : " + this.raisonSociale + ", " : "";
		String adresse = this.adresse != null ? "adresse : " + this.adresse + ", " : "";
		String ice = this.ice != null ? "ice : " + this.ice + ", " : "";
		String ifu = this.ifu != null ? "ifu : " + this.ifu + ", " : "";
		String cnss = this.cnss != null ? "cnss : " + this.cnss + ", " : "";
		String numTaxe = this.numTaxe != null ? "numTaxe : " + this.numTaxe + ", " : "";
		
		String nomMand = this.nomMandataire != null ? "nom : " + this.nomMandataire + ", " : "";
		String prenomMand = this.prenomMandataire != null ? "prenom : " + this.prenomMandataire + ", " : "";
		String cinMand = this.cinMandataire != null ? "cin : " + this.cinMandataire + ", " : "";
		String fonctMand = this.fonctMandataire != null ? "fonction : " + this.fonctMandataire + ", " : "";
		String telMand = this.telMandataire != null ? "tel : " + this.telMandataire + ", " : "";
		
		String nomGest = this.nomGestionnaire != null ? "nom : " + this.nomGestionnaire + ", " : "";
		String prenomGest = this.prenomGestionnaire != null ? "prenom : " + this.prenomGestionnaire + ", " : "";
		String emailGest = this.emailGestionnaire != null ? "cin : " + this.emailGestionnaire + ", " : "";
		String fonctGest = this.fonctGestionnaire != null ? "fonction : " + this.fonctGestionnaire + ", " : "";
		String telGest = this.telGestionnaire != null ? "tel : " + this.telGestionnaire + ", " : "";
		
		return "Adhesion [ id : " + this.id + numRC + raisonSociale + adresse + ice + ifu + cnss + numTaxe
				+ " Mandataire : ["+ nomMand + prenomMand + cinMand + fonctMand + telMand + "]"
				+ " Gestionnaire : ["+ nomGest + prenomGest + emailGest + fonctGest + telGest + "]]";
	}
}
