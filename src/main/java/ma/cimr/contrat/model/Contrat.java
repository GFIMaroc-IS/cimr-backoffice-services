package ma.cimr.contrat.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTRAT") 
public class Contrat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6204880060525978338L;

	@Id
	@Column(name="ID_ADHESION")
	private Long id;
	
	@OneToOne
	@MapsId
	@JoinColumn(name="ID_ADHESION")
	private Adhesion adhesion;
	
	@Column(name="DATE_EFFET_SOUSCRIPTION")
	private Date dateEffetSouscription;
	
	@Column(name="EFFECTIF")
	private Integer effectif;
	
	@Column(name="TITULAIRE")
	private String titulaire;
	
	@Column(name="RIB")
	private String rib;
	
	@ManyToOne(optional=true)
	@JoinColumn(name="ID_MODE_PAIEMENT")
	private RefModePaiement modePaiement;
	
	@ManyToOne
	@JoinColumn(name="ID_PRODUIT")
	private RefProduit produit;
	
	@OneToMany(mappedBy="contrat", cascade=CascadeType.ALL)
	private List<Delegataire> delegataires;
	
	@Column(name="RIB_FILE")
	private Byte[] ribFile;
	
	@Column(name="STATUT")
	private Integer statut;
	
	@Column(name="MOTIF_REJET")
	private String motifRejet;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateEffetSouscription() {
		return dateEffetSouscription;
	}

	public void setDateEffetSouscription(Date dateEffetSouscription) {
		this.dateEffetSouscription = dateEffetSouscription;
	}

	public Integer getEffectif() {
		return effectif;
	}

	public void setEffectif(Integer effectif) {
		this.effectif = effectif;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public RefModePaiement getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(RefModePaiement modePaiement) {
		this.modePaiement = modePaiement;
	}

	public RefProduit getProduit() {
		return produit;
	}

	public void setProduit(RefProduit produit) {
		this.produit = produit;
	}

	public List<Delegataire> getDelegataires() {
		return delegataires;
	}

	public void setDelegataires(List<Delegataire> delegataires) {
		this.delegataires = delegataires;
	}

	public Byte[] getRibFile() {
		return ribFile;
	}

	public void setRibFile(Byte[] ribFile) {
		this.ribFile = ribFile;
	}

	public Adhesion getAdhesion() {
		return adhesion;
	}

	public void setAdhesion(Adhesion adhesion) {
		this.adhesion = adhesion;
	}

	public Integer getStatut() {
		return statut;
	}

	public void setStatut(Integer statut) {
		this.statut = statut;
	}

	public String getMotifRejet() {
		return motifRejet;
	}

	public void setMotifRejet(String motifRejet) {
		this.motifRejet = motifRejet;
	}

	@Override
	public String toString() {
		String dateEffetSouscription = this.dateEffetSouscription != null ? "dateEffetSouscription : " + this.dateEffetSouscription + ", " : "";
		String effectif = this.effectif != null ? "effectif : " + this.effectif + ", " : "";
		String titulaire = this.titulaire != null ? "titulaire : " + this.titulaire + ", " : "";
		String rib = this.rib != null ? "rib : " + this.rib + ", " : "";
		String produit = this.produit != null && this.produit.getLibelle() != null ? "produit : " + this.produit.getLibelle() + ", " : "";
		String modePaiement = this.modePaiement != null && this.modePaiement.getLibelle() != null ? "modePaiement : " + this.modePaiement.getLibelle() + ", " : "";
		
		return "Contrat [ " + dateEffetSouscription + effectif + titulaire + rib + produit + modePaiement + "]";
	}
}
