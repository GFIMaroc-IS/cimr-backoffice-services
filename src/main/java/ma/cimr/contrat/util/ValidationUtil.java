package ma.cimr.contrat.util;

public class ValidationUtil {

	public static Boolean isValidTelephoneNumber(String tel) {
		if (tel != null && tel.matches("[0-9]{10}")) {
			return true;
		}
		return false;

	}
	
	public static Boolean isValidEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
