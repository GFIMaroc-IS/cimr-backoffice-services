package ma.cimr.contrat.util;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ApiUtils {

	public static JSONObject post(JSONObject request, String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> apiResponse = restTemplate.exchange(urlString, HttpMethod.POST, entity, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.CREATED) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
	
	public static JSONObject get(String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<String> apiResponse = restTemplate.getForEntity(urlString, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.OK) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
}
