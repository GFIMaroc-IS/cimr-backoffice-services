package ma.cimr.contrat.rest.Authentication;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.config.JwtTokenUtil;
import ma.cimr.contrat.config.UtilisateurDetails;
import ma.cimr.contrat.service.IAuthenticationService;
import ma.cimr.contrat.service.dto.Authentication.JwtRequest;
import ma.cimr.contrat.service.dto.Authentication.JwtResponse;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private IAuthenticationService authenticationService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	MessageSource messageSource;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationController.class);

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> generateAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		boolean can = false;
		Map<String, Object> response = new HashMap<>();
		String login = authenticationRequest.getUsername();
		String password = authenticationRequest.getPassword();
		List<String> messages = new ArrayList<>();

		try {
			Objects.requireNonNull(authenticationRequest.getUsername());
			Objects.requireNonNull(authenticationRequest.getPassword());
		} catch(NullPointerException e) {
			logger.error("login/mot de passe null", e);
			messages.add(messageSource.getMessage("code.auth.002", null, Locale.getDefault()));
			response.put(Constants.ERRORS, messages);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		

		try {
			boolean isLogged = authenticationService.checkUser(login, password);

			if (!isLogged) {
				logger.info("User can not be Authenticated [" + login + "]");

				messages.add(messageSource.getMessage("code.auth.001", null, Locale.getDefault()));

				response.put(Constants.ERRORS, messages);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}

			UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(login);

			if (userDetails != null) {
				logger.info("Authentification de l'utilisateur : " + login + " , heure : " + new Date().toString());
				can = Boolean.TRUE;
			}
			if (can) {
				UtilisateurDetails user = (UtilisateurDetails) userDetails;
				final String token = jwtTokenUtil.generateToken(userDetails);

				return ResponseEntity
						.ok(new JwtResponse(token, authenticationRequest.getUsername(), user.getAuthorities()));

			} else {
				messages.add(messageSource.getMessage("code.auth.001", null, Locale.getDefault()));
				response.put(Constants.ERRORS, messages);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}

		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

}
