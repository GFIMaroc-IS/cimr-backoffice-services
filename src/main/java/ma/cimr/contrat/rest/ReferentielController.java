package ma.cimr.contrat.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ma.cimr.contrat.service.IReferentielService;
import ma.cimr.contrat.service.dto.RefCategorieHabilitationDto;
import ma.cimr.contrat.service.dto.RefModePaiementDto;
import ma.cimr.contrat.service.dto.RefProduitDto;


@RestController
@RequestMapping("/api/referentiel")
public class ReferentielController {

	@Autowired
	private IReferentielService referentielService;
	
	@GetMapping("/categoriesHabilitation")
	public List<RefCategorieHabilitationDto> getListeCategories() {
		return referentielService.getCategoriesHabilitation();
	}
	
	@GetMapping("/modesPaiement")
	public List<RefModePaiementDto> getListeModesPaiement() {
		return referentielService.getModesPaiement();
	}
	
	@GetMapping("/produits")
	public List<RefProduitDto> getListeProduits() {
		return referentielService.getProduits();
	}
		
	public RefModePaiementDto getModePaiement(@RequestParam(name="code") String code) {
		return referentielService.getModePaiementByCode(code);
	}
		
	public RefProduitDto getProduit(@RequestParam(name="code") String code) {
		return referentielService.getProduitByCode(code);
	}
	
	@GetMapping("/findModePaiement")	
	public RefModePaiementDto getModePaiementById(@RequestParam(name="id") Long id) {
		return referentielService.getModePaiementById(id);
	}
	
	@GetMapping("/findProduit")	
	public RefProduitDto getProduitById(@RequestParam(name="id") Long id) {
		return referentielService.getProduitById(id);
	}
	
}
