package ma.cimr.contrat.rest.Adhesion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.service.IAdhesionService;


@RestController
@RequestMapping("/bo/adhesion")
public class AdhesionController {

	private static final Logger logger = LoggerFactory.getLogger(AdhesionController.class);
	
	@Autowired
	private IAdhesionService adhesionService;
	
	
//	@GetMapping("/listDemandesAdhesionValidateur")
//    public List<DemandeAdhesionDto> listDemandesAdhesionValidateur(){
//		
//		List<DemandeAdhesionDto> demandes = adhesionService.getDemandesAdhesionValidateur();
//		if(demandes != null) {
//			return demandes;
//		}
//		return new ArrayList<DemandeAdhesionDto>();
//	}
//	
//	@GetMapping("/listDemandesAdhesionApprobateur")
//    public List<DemandeAdhesionDto> listDemandesAdhesionApprobateur(){
//		
//		List<DemandeAdhesionDto> demandes = adhesionService.getDemandesAdhesionApprobateur();
//		if(demandes != null) {
//			return demandes;
//		}
//		return new ArrayList<DemandeAdhesionDto>();
//	}
	
	@GetMapping("/listDemandesAdhesion")
    public Map<String, Object> listDemandesAdhesion(@RequestParam(name="profil", required = true) String profil, 
    													 @RequestParam(name="ifu", required = false) String ifu, 
    													 @RequestParam(name="dateDu", required = false) String dateDu,
    													 @RequestParam(name="dateAu", required = false) String dateAu,
    													 @RequestParam(name="statut", required = false) Integer statut,
    													 @RequestParam(name="page", required = true) Integer page){
		
		Map<String, Object> response = adhesionService.getDemandesAdhesion(profil, ifu, dateDu, dateAu, statut, page, true);
		
		return response;
	}
	
	@GetMapping("/detailDemandeAdhesion")
    public Map<String, Object> detailDemandeAdhesion(@RequestParam(name="id") Long idAdhesion){
		
		Map<String, Object> response = adhesionService.getDetailDemande(idAdhesion);
		
		return response;
	}
	
	@GetMapping("/validateDemandeAdhesion")
    public ResponseEntity<Map<String,Object>> validateDemandeAdhesion(@RequestParam(name="id") Long idAdhesion){
		
		Map<String,Object> response = new HashMap<String, Object>();
		List<String> errors = new ArrayList<String>();
		try {
			Boolean isValid = adhesionService.validateDemande(idAdhesion);
			if(isValid) {
				// generer login/password
				Map<String, Object> credentials = adhesionService.generateCredentials(idAdhesion);
				
				if(credentials != null) {
					String login = (String)credentials.get(Constants.LOGIN);
					String password = (String)credentials.get(Constants.PASSWORD);
					// notifier adherent
					Boolean notified = adhesionService.notifyAdherent(idAdhesion, login, password);
					if(notified) {
						return new ResponseEntity<>(response, HttpStatus.OK);
					} else {
						errors.add("Une erreur est survenue lors de la notification de l'adhérent.");
					}
				} else {
					errors.add("Une erreur est survenue lors de la génération du login/mot de passe.");
				}
			}
		} catch (JSONException e) {
			logger.error("method validateDemandeAdhesion, JSONException : ", e);
			e.printStackTrace();
		}
		response.put(Constants.ERRORS, errors);
		return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
	}
	
	@GetMapping("/rejectDemandeAdhesion")
    public Boolean rejectDemandeAdhesion(@RequestParam(name="id") Long idAdhesion, @RequestParam(name="motif") String motif){
		
		Boolean isRejected = adhesionService.rejectDemande(idAdhesion, motif);
		
		return isRejected;
	}
	
}
