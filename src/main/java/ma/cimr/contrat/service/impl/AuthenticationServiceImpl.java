package ma.cimr.contrat.service.impl;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.dao.UtilisateurRepository;
import ma.cimr.contrat.model.Utilisateur;
import ma.cimr.contrat.service.IAuthenticationService;
import ma.cimr.contrat.util.StringUtil;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public boolean checkUser(String username, String password) {
		
		byte[] bytesEncoded = Base64.getEncoder().encode(password.getBytes());
		String passwordEncoded = new String(bytesEncoded);
		

		Utilisateur utilisateur = utilisateurRepository.getUtilisateur(username, passwordEncoded);
		
		if(utilisateur != null) {
			logger.info("(User, Password) = ('" + username + "', '" + password.charAt(0) + "******') exists");
			return true;
		}
			
		return false;
	}

	@Override
	public boolean saveUser(String username, String password) {
		
		String passwordEncoded = StringUtil.encodeStringBCrypt(password);
		Utilisateur utilisateur = new Utilisateur(username, passwordEncoded);
		
		Utilisateur entity = utilisateurRepository.save(utilisateur);
		if(entity != null && entity.getId() != null)
			return true;
		
		return false;
	}


}
