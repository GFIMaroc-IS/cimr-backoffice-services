package ma.cimr.contrat.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.dao.AdhesionRepository;
import ma.cimr.contrat.dao.ContratRepository;
import ma.cimr.contrat.model.Adhesion;
import ma.cimr.contrat.model.Contrat;
import ma.cimr.contrat.service.EmailService;
import ma.cimr.contrat.service.IAdhesionService;
import ma.cimr.contrat.service.dto.AdhesionDto;
import ma.cimr.contrat.service.dto.ContratDto;
import ma.cimr.contrat.service.dto.DemandeAdhesionDto;
import ma.cimr.contrat.service.dto.PieceJustifDto;
import ma.cimr.contrat.service.mapper.AdhesionMapper;
import ma.cimr.contrat.service.mapper.ContratMapper;
import ma.cimr.contrat.service.util.ServiceUtil;
import ma.cimr.contrat.util.ApiUtils;
import ma.cimr.contrat.util.DateUtil;
import ma.cimr.contrat.util.StringUtil;

@Service
public class AdhesionServiceImpl implements IAdhesionService {

	private static final Logger logger = LoggerFactory.getLogger(AdhesionServiceImpl.class);

	@Autowired
	private AdhesionRepository adhesionRepository;
	
	@Autowired
	private EmailService emailService;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AdhesionMapper adhesionMapper;
	
	@Autowired
	private ContratRepository contratRepository;
	
	@Autowired
	private ContratMapper contratMapper;
	
	@Override
	public List<DemandeAdhesionDto> getDemandesAdhesionValidateur() {
		
		List<DemandeAdhesionDto> demandes = null;
		
//		List<Object> demandesFromBPM = BPMService.getTaches();
		
		List<Integer> statuts = Arrays.asList(new Integer[]{Constants.STATUT_EN_COURS_CODE,
															Constants.STATUT_A_VALIDER_CODE,
															Constants.STATUT_REJETE_CODE});
		
		List<Adhesion> adhesions = adhesionRepository.findAllByStatut(statuts);
		
		if(adhesions != null && adhesions.size() > 0) {
			demandes = new ArrayList<>();
			for(Adhesion a : adhesions) {
				
				DemandeAdhesionDto demande = adhesionToDemandeAdhesion(a);
				
				demandes.add(demande);
				
			}
		}
		return demandes;
	}
	
	@Override
	public List<DemandeAdhesionDto> getDemandesAdhesionApprobateur() {
		
		List<DemandeAdhesionDto> demandes = null;
		
//		List<Object> demandesFromBPM = BPMService.getTaches();
		
		List<Integer> statuts = Arrays.asList(new Integer[]{Constants.STATUT_VALIDE_CODE,
															Constants.STATUT_A_VALIDER_CODE,
															Constants.STATUT_REJETE_CODE});
		
		List<Adhesion> adhesions = adhesionRepository.findAllByStatut(statuts);
		
		if(adhesions != null && adhesions.size() > 0) {
			demandes = new ArrayList<>();
			for(Adhesion a : adhesions) {
				
				DemandeAdhesionDto demande = adhesionToDemandeAdhesion(a);
				
				demandes.add(demande);
				
			}
		}
		return demandes;
	}
	
	@Override
	public Map<String, Object> getDemandesAdhesion(String profil, String ifu, String dateDu, String dateAu,
			Integer statut, Integer page, Boolean paging) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<DemandeAdhesionDto> demandes = null;
		
		StringBuilder  reqSelect = new StringBuilder("SELECT a.ID_ADHESION, a.ICE, a.IFU, a.RAISON_SOC, a.RC_FILE, c.DATE_EFFET_SOUSCRIPTION, c.STATUT ");
		StringBuilder reqSelectCountPaging = new StringBuilder("SELECT count(*) ");
		StringBuilder  reqFrom = new StringBuilder("FROM ADHESION a INNER JOIN CONTRAT c on a.ID_ADHESION = c.ID_ADHESION ");
		StringBuilder  reqWhere = new StringBuilder("");
		
		if(statut != null ) {
			reqWhere.append("WHERE c.STATUT = :statut ");
		
		} else if(!StringUtil.isEmpty(profil) ) {
			reqWhere.append("WHERE c.STATUT in :statuts ");
		}
		
		if(!StringUtil.isEmpty(ifu)) {
			reqWhere.append("AND a.IFU = :ifu ");
		}
		
		if(!StringUtil.isEmpty(dateDu)) {
			reqWhere.append("AND c.DATE_EFFET_SOUSCRIPTION >= :dateDu ");
		}
		
		if(!StringUtil.isEmpty(dateAu)) {
			reqWhere.append("AND c.DATE_EFFET_SOUSCRIPTION <= :dateAu ");
		}
		
		Query query = entityManager.createNativeQuery(reqSelect.toString() + reqFrom.toString() + reqWhere.toString());
		Query queryCountPaging = entityManager.createNativeQuery(reqSelectCountPaging.toString() + reqFrom.toString() + reqWhere.toString());
		
		fillQueryParams(profil, ifu, dateDu, dateAu, statut, query);
		fillQueryParams(profil, ifu, dateDu, dateAu, statut, queryCountPaging);
		
		if(paging){
			query.setFirstResult((page-1) * 10);
			query.setMaxResults(Constants.DB_MAX_RESULTS);
		}
		List<Object[]> list = query.getResultList();
		Integer sizePaging = ((BigInteger)queryCountPaging.getSingleResult()).intValue();
		Integer ceil = (int) Math.ceil(sizePaging / Constants.DB_MAX_RESULTS);
		Integer totalPages = (sizePaging % Constants.DB_MAX_RESULTS) == 0 ? ceil : ceil + 1;
		
		if(list != null && list.size() > 0) {
			demandes = new ArrayList<DemandeAdhesionDto>();
			
			int i = 0;
			if(paging) i = 0;
			
			for(Object[] obj : list){
				DemandeAdhesionDto demande = new DemandeAdhesionDto();
				
				demande.setId(Long.valueOf((Integer) obj[0+i]));
				demande.setNumIF((String) obj[2+i]);
				demande.setRaisonSociale((String) obj[3+i]);
				
				Date dateEffetSouscription = (Date) obj[5+i];
				demande.setDateEffetSouscription(dateEffetSouscription != null ? DateUtil.dateToString(dateEffetSouscription, Constants.DATE_FORMAT_FLUX)
							: "");
				demande.setStatut((Integer) obj[6+i]);
				demande.setStatutLibelle(ServiceUtil.getStatutLibelle((Integer) obj[6+i]));
				
				demandes.add(demande);
			}
		}
		
		result.put(Constants.LIST_DTO, demandes);
		result.put(Constants.TOTAL_PAGES, totalPages);
		
		return result;
	}
	
	private void fillQueryParams(String profil, String ifu, String dateDu, String dateAu, Integer statut, Query query) {
		
		if(statut != null ) {
			query.setParameter("statut", statut);
		
		} else if(!StringUtil.isEmpty(profil) ) {
			
			List<Integer> statuts = null;
			
			if(profil.equalsIgnoreCase(Constants.PROFIL_VALIDATEUR)) {
				statuts = Arrays.asList(new Integer[]{Constants.STATUT_EN_COURS_CODE,
						Constants.STATUT_A_VALIDER_CODE,
						Constants.STATUT_REJETE_CODE});
			
			} else if(profil.equalsIgnoreCase(Constants.PROFIL_APPROBATEUR)) {
				statuts = Arrays.asList(new Integer[]{Constants.STATUT_VALIDE_CODE,
						Constants.STATUT_A_VALIDER_CODE,
						Constants.STATUT_REJETE_CODE});
			}
			query.setParameter("statuts", statuts);
		}
		
		if(!StringUtil.isEmpty(ifu)) {
			query.setParameter("ifu", ifu);
		}
		
		if(!StringUtil.isEmpty(dateDu)) {
			query.setParameter("dateDu", dateDu);
		}
		
		if(!StringUtil.isEmpty(dateAu)) {
			query.setParameter("dateAu", dateAu);
		}

	}

	@Override
	public Map<String, Object> getDetailDemande(Long id) {
		
		Map<String, Object> response = new HashMap<>();
		
		DemandeAdhesionDto demande = null;
		List<PieceJustifDto> pieces = new ArrayList<PieceJustifDto>();
		AdhesionDto adhesionDto = null;
		ContratDto  contratDto  = null;
		
		Adhesion adhesion = adhesionRepository.getOne(id);
		Optional<Contrat> oContrat   = contratRepository.findById(id);
		if(adhesion != null) {
			demande = adhesionToDemandeAdhesion(adhesion);
			adhesionDto = adhesionMapper.toDto(adhesion);
			PieceJustifDto piece = new PieceJustifDto("PIECE 1" ,1,"Non approuvé" );
			PieceJustifDto piece2 = new PieceJustifDto("PIECE 2",1,"Non approuvé" );
			pieces.add(piece);
			pieces.add(piece2);
		}
		
		if (oContrat.isPresent()) {
			contratDto = contratMapper.toDto(oContrat.get());
		}
		
		response.put("demande", demande);
		response.put("pieces", pieces);
		response.put("adhesion", adhesionDto);
		response.put("contrat", contratDto);
		
		return response;
	}
	
	private DemandeAdhesionDto adhesionToDemandeAdhesion(Adhesion a) {
		
		DemandeAdhesionDto demande = new DemandeAdhesionDto();
		
		demande.setId(a.getId());
		demande.setNumIF(!StringUtil.isEmpty(a.getIfu()) ? a.getIfu() : "");
		demande.setRaisonSociale(!StringUtil.isEmpty(a.getRaisonSociale()) ? a.getRaisonSociale() : "");
		if(a.getContrat() != null) {
			Date dateEffetSouscription = a.getContrat().getDateEffetSouscription();
			demande.setDateEffetSouscription(dateEffetSouscription != null ? DateUtil.dateToString(dateEffetSouscription, Constants.DATE_FORMAT_FLUX)
					: "");
			demande.setStatut(a.getContrat().getStatut());
			demande.setStatutLibelle(ServiceUtil.getStatutLibelle(a.getContrat().getStatut()));
		} else {
			demande.setDateEffetSouscription("");
			demande.setStatutLibelle("");
		}
		
		return demande;
	}

	@Override
	public Boolean validateDemande(Long id) {
		
		Boolean isValid = false;
		
		Adhesion adhesion = adhesionRepository.getOne(id);
		if(adhesion != null) {
			adhesion.getContrat().setStatut(Constants.STATUT_VALIDE_CODE);
			adhesionRepository.save(adhesion);
			isValid = true;
		}
		
		return isValid;
	}

	@Override
	public Boolean rejectDemande(Long id, String motif) {

		Boolean isRejected = false;
		
		Adhesion adhesion = adhesionRepository.getOne(id);
		if(adhesion != null) {
			adhesion.getContrat().setStatut(Constants.STATUT_REJETE_CODE);
			adhesion.getContrat().setMotifRejet(motif);
			adhesionRepository.save(adhesion);
			isRejected = true;
		}
		
		return isRejected;
	}

	@Override
	public Map<String, Object> generateCredentials(Long idAdhesion) throws JSONException {

		Map<String, Object> credentials = null;
		
		String url = ApplicationConfig.getUrlGenerateCredentials() + idAdhesion;
		logger.info("generateCredentials va appeler la requête : #" + url + "#");
		JSONObject response = ApiUtils.get(url);
		
		if(response != null) {
			String login = response.getString(Constants.LOGIN);
			String password = response.getString(Constants.PASSWORD);
			
			byte[] bytesDecoded= Base64.getDecoder().decode(password);
			String passwordDecoded = new String(bytesDecoded);
			
			credentials = new HashMap<String, Object>();
			credentials.put(Constants.LOGIN, login);
			credentials.put(Constants.PASSWORD, passwordDecoded);
		}
		
		return credentials;
	}

	@Override
	public Boolean notifyAdherent(Long idAdhesion, String login, String password) {
		logger.info("Le mail de notification va être envoyé au nouvel adhérent" + login);
		List<String> addresses = new ArrayList<>();
		try {
			addresses.add(login);
			emailService.sendMail(login, password, addresses);
			return true;
		} catch(Exception e){
			logger.error("Error in method notifyAdherent, ", e);
		}
		
		return false;
	}

	
	

}
