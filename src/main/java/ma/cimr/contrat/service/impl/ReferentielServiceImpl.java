package ma.cimr.contrat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.dao.RefCategorieHabilitationRepository;
import ma.cimr.contrat.dao.RefModePaiementRepository;
import ma.cimr.contrat.dao.RefProduitRepository;
import ma.cimr.contrat.model.RefCategorieHabilitation;
import ma.cimr.contrat.model.RefModePaiement;
import ma.cimr.contrat.model.RefProduit;
import ma.cimr.contrat.service.IReferentielService;
import ma.cimr.contrat.service.dto.RefCategorieHabilitationDto;
import ma.cimr.contrat.service.dto.RefModePaiementDto;
import ma.cimr.contrat.service.dto.RefProduitDto;
import ma.cimr.contrat.util.CollectionUtil;

@Service
public class ReferentielServiceImpl implements IReferentielService{

	private static final Logger logger = LoggerFactory.getLogger(ReferentielServiceImpl.class);
	
	@Autowired
	private RefCategorieHabilitationRepository refCategorieHabilitationRepository;
	@Autowired
	private RefModePaiementRepository refModePaiementRepository;
	@Autowired
	private RefProduitRepository refProduitRepository;

	@Override
	public List<RefCategorieHabilitationDto> getCategoriesHabilitation() {
		logger.info("Récupérer la liste des catégories d'habilitation");
		List<RefCategorieHabilitationDto> dtos = new ArrayList<>();
		List<RefCategorieHabilitation> categories = refCategorieHabilitationRepository.findAll();
		if(!CollectionUtil.isEmpty(categories)) {
			categories.forEach(t->dtos.add(new RefCategorieHabilitationDto(t.getId(), t.getCode(), t.getLibelle())));
		}
		return dtos;
	}

	@Override
	public List<RefModePaiementDto> getModesPaiement() {
		logger.info("Récupérer la liste des modes de paiement");
		List<RefModePaiementDto> dtos = new ArrayList<>();
		List<RefModePaiement> modes = refModePaiementRepository.findAll();
		if(!CollectionUtil.isEmpty(modes)) {
			modes.forEach(t->dtos.add(new RefModePaiementDto(t.getId(), t.getCode(), t.getLibelle())));
		}
		return dtos;
	}

	@Override
	public List<RefProduitDto> getProduits() {
		logger.info("Récupérer la liste des produits");
		List<RefProduitDto> dtos = new ArrayList<>();
		List<RefProduit> produits = refProduitRepository.findAll();
		if(!CollectionUtil.isEmpty(produits)) {
			produits.forEach(t->dtos.add(new RefProduitDto(t.getId(), t.getCode(), t.getLibelle())));
		}
		return dtos;
	}

	@Override
	public RefModePaiementDto getModePaiementByCode(String code) {
		
		RefModePaiementDto dto = null;
		RefModePaiement modePaiement = refModePaiementRepository.findByCode(code);
		if(modePaiement != null) {
			dto = new RefModePaiementDto(code, modePaiement.getLibelle());
		}
		return dto;
	}

	@Override
	public RefProduitDto getProduitByCode(String code) {
		
		RefProduitDto dto = null;
		RefProduit produit = refProduitRepository.findByCode(code);
		if(produit != null) {
			dto = new RefProduitDto(code, produit.getLibelle());
		}
		return dto;
	}
	
	@Override
	public RefModePaiementDto getModePaiementById(Long id) {
		
		RefModePaiementDto dto = null;
		RefModePaiement modePaiement = refModePaiementRepository.getOne(id);
		if(modePaiement != null) {
			dto = new RefModePaiementDto(modePaiement.getCode(), modePaiement.getLibelle());
		}
		return dto;
	}

	@Override
	public RefProduitDto getProduitById(Long id) {
		
		RefProduitDto dto = null;
		RefProduit produit = refProduitRepository.getOne(id);
		if(produit != null) {
			dto = new RefProduitDto(produit.getCode(), produit.getLibelle());
		}
		return dto;
	}
	
}
