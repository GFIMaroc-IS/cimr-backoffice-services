package ma.cimr.contrat.service.impl;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.service.EmailService;

@Component
public class EmailServiceImpl implements EmailService {

	private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private MessageSource messageSource;

	@Override
	public void sendSimpleMessage(String to, String subject, String text) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(ApplicationConfig.getMailContact());
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		
		emailSender.send(message);
	}

	@Override
	public void sendMail(String login, String password, List<String> addresses) throws Exception{
        
		StringBuffer buf = new StringBuffer();
		StringBuffer bufLog = new StringBuffer();
        String newline = System.getProperty("line.separator");
        
        buf.append(messageSource.getMessage("notify.accueil", null, Locale.getDefault())).append(newline).append(newline);
        bufLog.append(messageSource.getMessage("notify.accueil", null, Locale.getDefault())).append(newline).append(newline);
        
        buf.append(messageSource.getMessage("notify.msg1", null, Locale.getDefault())).append(newline).append(newline);
        bufLog.append(messageSource.getMessage("notify.msg1", null, Locale.getDefault())).append(newline).append(newline);
        
        buf.append("Login/Mot de passe : ").append(login).append(" / ").append(password).append(newline).append(newline).append(newline);
        bufLog.append("Login/Mot de passe : ").append(login).append(" / ").append("********").append(newline).append(newline).append(newline);
        
        buf.append(messageSource.getMessage("notify.msg2", null, Locale.getDefault()));
        bufLog.append(messageSource.getMessage("notify.msg2", null, Locale.getDefault()));
        
        StringBuilder to = new StringBuilder();
        to.append(addresses);
        
        if (ApplicationConfig.isNotificationActivated()) {
               logger.info("sending message to " + to + " message : " + bufLog.toString());
               Thread thread = new Thread(new Runnable() {
                      @Override
                      public void run() {
                             for(String address : addresses) {
                            	 sendSimpleMessage(address,messageSource.getMessage("notify.subject", null, Locale.getDefault()), buf.toString());
                             }
                             
                             
                      }
               });
               thread.start();
        } else {
               logger.info("Mail Not Activated");
        }
	}
}
