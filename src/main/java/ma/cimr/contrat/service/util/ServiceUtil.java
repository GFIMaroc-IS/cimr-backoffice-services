package ma.cimr.contrat.service.util;

import ma.cimr.contrat.common.Constants;

public class ServiceUtil {

	public static String getStatutLibelle(Integer code) {
		if (code == null) {
			return "";
		}
		
		switch (code) {
		case 1:
			return Constants.STATUT_EN_COURS;
		case 2:
			return Constants.STATUT_A_VALIDER;
		case 3:
			return Constants.STATUT_VALIDE;
		case 4:
			return Constants.STATUT_REJETE;
			
		default:
			return "";
		}

	}
}
