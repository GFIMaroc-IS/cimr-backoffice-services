package ma.cimr.contrat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.config.UtilisateurDetails;
import ma.cimr.contrat.dao.ProfilRepository;
import ma.cimr.contrat.dao.UtilisateurRepository;
import ma.cimr.contrat.model.Profil;
import ma.cimr.contrat.model.Utilisateur;


@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private ProfilRepository profilRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		logger.info("==== Début de la méthode loadUserByUsername username : "+ username + " ====");
		UserDetails userDetails = null;
		
		try {
			Utilisateur backUser = utilisateurRepository.getUtilisateur(username);
			if (backUser != null && backUser.getUsername() != null && !backUser.getUsername().isEmpty()) {
				
				List<Profil> profils = profilRepository.getProfilsByUsername(username);
				
				userDetails = new UtilisateurDetails(username, profils);
			}
		} catch(Exception e) {
			logger.error("Exception in method loadUserByUsername : " + username, e);
		}
		 
		return userDetails;
	}

}