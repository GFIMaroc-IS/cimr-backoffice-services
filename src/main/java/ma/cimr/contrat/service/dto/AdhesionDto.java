package ma.cimr.contrat.service.dto;

public class AdhesionDto {

	private Long id;
	private String numRC;
	private String raisonSociale;
	private String adresse;
	private String formeJuridique;
	private String ice;
	private String ifu;
	private String cnss;
	private String numTaxe;
	private String nomMandataire;
	private String prenomMandataire;
	private String cinMandataire;
	private String fonctMandataire;
	private String telMandataire;
	private String nomGestionnaire;
	private String prenomGestionnaire;
	private String emailGestionnaire;
	private String fonctGestionnaire;
	private String telGestionnaire;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumRC() {
		return numRC;
	}

	public void setNumRC(String numRC) {
		this.numRC = numRC;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getFormeJuridique() {
		return formeJuridique;
	}

	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}

	public String getIce() {
		return ice;
	}

	public void setIce(String ice) {
		this.ice = ice;
	}

	public String getIfu() {
		return ifu;
	}

	public void setIfu(String ifu) {
		this.ifu = ifu;
	}

	public String getCnss() {
		return cnss;
	}

	public void setCnss(String cnss) {
		this.cnss = cnss;
	}

	public String getNumTaxe() {
		return numTaxe;
	}

	public void setNumTaxe(String numTaxe) {
		this.numTaxe = numTaxe;
	}

	public String getNomMandataire() {
		return nomMandataire;
	}

	public void setNomMandataire(String nomMandataire) {
		this.nomMandataire = nomMandataire;
	}

	public String getPrenomMandataire() {
		return prenomMandataire;
	}

	public void setPrenomMandataire(String prenomMandataire) {
		this.prenomMandataire = prenomMandataire;
	}

	public String getCinMandataire() {
		return cinMandataire;
	}

	public void setCinMandataire(String cinMandataire) {
		this.cinMandataire = cinMandataire;
	}

	public String getFonctMandataire() {
		return fonctMandataire;
	}

	public void setFonctMandataire(String fonctMandataire) {
		this.fonctMandataire = fonctMandataire;
	}

	public String getTelMandataire() {
		return telMandataire;
	}

	public void setTelMandataire(String telMandataire) {
		this.telMandataire = telMandataire;
	}

	public String getNomGestionnaire() {
		return nomGestionnaire;
	}

	public void setNomGestionnaire(String nomGestionnaire) {
		this.nomGestionnaire = nomGestionnaire;
	}

	public String getPrenomGestionnaire() {
		return prenomGestionnaire;
	}

	public void setPrenomGestionnaire(String prenomGestionnaire) {
		this.prenomGestionnaire = prenomGestionnaire;
	}

	public String getEmailGestionnaire() {
		return emailGestionnaire;
	}

	public void setEmailGestionnaire(String emailGestionnaire) {
		this.emailGestionnaire = emailGestionnaire;
	}

	public String getFonctGestionnaire() {
		return fonctGestionnaire;
	}

	public void setFonctGestionnaire(String fonctGestionnaire) {
		this.fonctGestionnaire = fonctGestionnaire;
	}

	public String getTelGestionnaire() {
		return telGestionnaire;
	}

	public void setTelGestionnaire(String telGestionnaire) {
		this.telGestionnaire = telGestionnaire;
	}

	

}
