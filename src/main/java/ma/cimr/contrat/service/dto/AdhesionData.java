package ma.cimr.contrat.service.dto;

public class AdhesionData {
	
	private AdhesionDto adhesion;
	private ContratDto contrat;
	
	public AdhesionDto getAdhesion() {
		return adhesion;
	}
	public void setAdhesion(AdhesionDto adhesion) {
		this.adhesion = adhesion;
	}
	public ContratDto getContrat() {
		return contrat;
	}
	public void setContrat(ContratDto contrat) {
		this.contrat = contrat;
	}
}
