package ma.cimr.contrat.service.dto;

public class RefModePaiementDto {

	private Long id;
	private String code;
	private String libelle;

	public RefModePaiementDto(String code, String libelle) {
		this.code = code;
		this.libelle = libelle;
	}
	
	public RefModePaiementDto(Long id, String code, String libelle) {
		this.id = id;
		this.code = code;
		this.libelle = libelle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
