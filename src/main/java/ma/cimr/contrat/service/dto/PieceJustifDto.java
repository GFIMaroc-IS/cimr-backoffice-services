package ma.cimr.contrat.service.dto;

public class PieceJustifDto {

	private Long id;
	private byte[] piece;
	private String nom;
	private Integer statut;
	private String statutLibelle;
	private String motifRejet;
	
	public PieceJustifDto() {}

	public PieceJustifDto(String nom, Integer statut, String statutLibelle) {
		super();
		this.nom = nom;
		this.statut = statut;
		this.statutLibelle = statutLibelle;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatut() {
		return statut;
	}

	public void setStatut(Integer statut) {
		this.statut = statut;
	}

	public String getStatutLibelle() {
		return statutLibelle;
	}

	public void setStatutLibelle(String statutLibelle) {
		this.statutLibelle = statutLibelle;
	}

	public byte[] getPiece() {
		return piece;
	}

	public void setPiece(byte[] piece) {
		this.piece = piece;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getMotifRejet() {
		return motifRejet;
	}

	public void setMotifRejet(String motifRejet) {
		this.motifRejet = motifRejet;
	}

	

}
