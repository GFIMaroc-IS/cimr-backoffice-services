package ma.cimr.contrat.service.dto.Authentication;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class JwtResponse {

	private final String jwttoken;
	private final String username;
	private Collection<? extends GrantedAuthority> grantedAuthorityList = new ArrayList<>();

	public String getToken() {
		return this.jwttoken;
	}
	
	public JwtResponse(String jwttoken, String username, Collection<? extends GrantedAuthority> grantedAuthorityList) {
		this.jwttoken = jwttoken;
		this.username = username;
		this.grantedAuthorityList = grantedAuthorityList;
	}

	public Collection<? extends GrantedAuthority> getGrantedAuthorityList() {
		return grantedAuthorityList;
	}

	public String getJwttoken() {
		return jwttoken;
	}
	
	public String getUsername() {
		return username;
	}


}