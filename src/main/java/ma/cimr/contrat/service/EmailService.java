package ma.cimr.contrat.service;

import java.util.List;

public interface EmailService {

    public void sendSimpleMessage(String to, String subject, String text);
    public void sendMail(String login, String password, List<String> addresses) throws Exception;
}
