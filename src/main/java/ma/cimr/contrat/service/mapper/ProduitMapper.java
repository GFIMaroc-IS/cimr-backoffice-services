package ma.cimr.contrat.service.mapper;

import org.mapstruct.Mapper;

import ma.cimr.contrat.model.RefProduit;
import ma.cimr.contrat.service.dto.RefProduitDto;

@Mapper(componentModel="spring")
public interface ProduitMapper extends EntityMapper<RefProduitDto, RefProduit> {
	
	@Override
	public RefProduit toEntity(RefProduitDto dto);
	
	@Override
    public RefProduitDto toDto(RefProduit entity);

}
