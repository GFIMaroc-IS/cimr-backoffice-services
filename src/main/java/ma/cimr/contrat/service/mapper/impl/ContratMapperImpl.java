package ma.cimr.contrat.service.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ma.cimr.contrat.dao.ContratRepository;
import ma.cimr.contrat.model.Contrat;
import ma.cimr.contrat.model.Delegataire;
import ma.cimr.contrat.service.dto.ContratDto;
import ma.cimr.contrat.service.mapper.ContratMapper;

public class ContratMapperImpl implements ContratMapper{

	@Autowired
	ContratRepository contratRepository;
	
	@Override
	public List<Contrat> toEntity(List<ContratDto> dtoList) {
		   if ( dtoList == null ) {
	            return null;
	        }

	        List<Contrat> list = new ArrayList<>();
	        for ( ContratDto contratDto : dtoList ) {
	            list.add( toEntity( contratDto ) );
	        }

	        return list;
	}

	@Override
	public List<ContratDto> toDto(List<Contrat> entityList) {
		if ( entityList == null ) {
            return null;
        }

        List<ContratDto> list = new ArrayList<>();
        for ( Contrat contrat : entityList ) {
            list.add( toDto( contrat ) );
        }

        return list;
	}

	@Override
	public Contrat toEntity(ContratDto dto) {
		
		List<Delegataire> delegataires = new ArrayList<>();
    	
        if ( dto == null ) {
            return null;
        }
        Contrat contrat = null;
        if(dto.getId() != null) {
        	contrat = contratRepository.getOne(dto.getId());
        } else {
        	contrat = new Contrat();
        }
        
        
        
        return contrat;
	}

	@Override
	public ContratDto toDto(Contrat entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
