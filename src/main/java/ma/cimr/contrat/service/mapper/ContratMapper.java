package ma.cimr.contrat.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import ma.cimr.contrat.model.Contrat;
import ma.cimr.contrat.model.Delegataire;
import ma.cimr.contrat.model.RefProduit;
import ma.cimr.contrat.service.dto.ContratDto;
import ma.cimr.contrat.service.dto.DelegataireDto;
import ma.cimr.contrat.service.dto.RefProduitDto;

@Mapper(componentModel="spring")
public interface ContratMapper extends EntityMapper<ContratDto, Contrat> {
	
	@Mapping(target = "modePaiement.id", source="modePaiementCode")
	@Mapping(target = "produit", source="produit")
	@Mapping(target = "dateEffetSouscription", expression="java(ma.cimr.contrat.util.DateUtil.stringToDate(dto.getDateEffetSouscription(),\"\"))")
	@Override
	public Contrat toEntity(ContratDto dto);
	
//	@Mapping(target = "delegataires", expression="map2(entity.getDelegataires())")
	@Override
	@Mapping(target = "produit", source="produit")
    public ContratDto toDto(Contrat entity);
	
	public default List<Delegataire> map(List<DelegataireDto> dtoList){
		return new DelegataireMapperImpl().toEntity(dtoList);
	}
	
	public default List<DelegataireDto> map2(List<Delegataire> list){
		return new DelegataireMapperImpl().toDto(list);
	}
	
	public default RefProduit map(RefProduitDto produitDto) {return new ProduitMapperImpl().toEntity(produitDto);}
	public default RefProduitDto map2(RefProduit produit) {return new ProduitMapperImpl().toDto(produit);}
}
