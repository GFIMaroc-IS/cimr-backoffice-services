package ma.cimr.contrat.service.mapper;

import org.mapstruct.Mapper;

import ma.cimr.contrat.model.Adhesion;
import ma.cimr.contrat.service.dto.AdhesionDto;

@Mapper(componentModel="spring")
public interface AdhesionMapper extends EntityMapper<AdhesionDto, Adhesion> {
	
	@Override
	public Adhesion toEntity(AdhesionDto dto);
	
	@Override
    public AdhesionDto toDto(Adhesion entity);

}
