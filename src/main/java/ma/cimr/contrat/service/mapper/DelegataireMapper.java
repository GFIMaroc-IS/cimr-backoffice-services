package ma.cimr.contrat.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import ma.cimr.contrat.model.Delegataire;
import ma.cimr.contrat.service.dto.DelegataireDto;

@Mapper(componentModel="spring")
public interface DelegataireMapper extends EntityMapper<DelegataireDto, Delegataire> {
	
	@Mapping(target = "categorieHabilitation.id" , source = "categorieHabilitationCode")
	@Override
	public Delegataire toEntity(DelegataireDto dto);
	
	@Mapping(source = "categorieHabilitation.code" , target = "categorieHabilitationCode")
	@Mapping(source = "categorieHabilitation.libelle" , target = "categorieHabilitationLibelle")
	@Override
    public DelegataireDto toDto(Delegataire entity);

}
