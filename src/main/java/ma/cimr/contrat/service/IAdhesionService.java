package ma.cimr.contrat.service;

import java.util.List;
import java.util.Map;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.web.bind.annotation.RequestParam;

import ma.cimr.contrat.service.dto.DemandeAdhesionDto;

public interface IAdhesionService {

	public List<DemandeAdhesionDto> getDemandesAdhesionValidateur();
	
	public List<DemandeAdhesionDto> getDemandesAdhesionApprobateur();
	
	public Map<String, Object> getDemandesAdhesion(String profil, String ifu, String dateDu, String dateAu, Integer statut, Integer page, Boolean paging);
	
	public Map<String, Object> getDetailDemande(Long id);
	
	public Boolean validateDemande(Long id);
	
	public Boolean rejectDemande(Long id, String motif);
	
	public Map<String, Object> generateCredentials(Long idAdhesion) throws JSONException;
	
	public Boolean notifyAdherent(Long idAdhesion, String login, String password);
	
}
