package ma.cimr.contrat.service;

public interface IAuthenticationService {

	public boolean checkUser(String username, String password);
	
	public boolean saveUser(String username, String password);
	
}
