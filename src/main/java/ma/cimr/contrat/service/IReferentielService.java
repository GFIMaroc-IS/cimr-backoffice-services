package ma.cimr.contrat.service;

import java.util.List;

import ma.cimr.contrat.service.dto.RefCategorieHabilitationDto;
import ma.cimr.contrat.service.dto.RefModePaiementDto;
import ma.cimr.contrat.service.dto.RefProduitDto;

public interface IReferentielService {

	public List<RefCategorieHabilitationDto> getCategoriesHabilitation();
	public List<RefModePaiementDto> getModesPaiement();
	public List<RefProduitDto> getProduits();
	public RefModePaiementDto getModePaiementByCode(String code);
	public RefProduitDto getProduitByCode(String code);
	RefModePaiementDto getModePaiementById(Long id);
	RefProduitDto getProduitById(Long id);
}
