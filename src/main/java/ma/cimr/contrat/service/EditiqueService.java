package ma.cimr.contrat.service;

import java.io.File;
import java.io.FileNotFoundException;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.util.FileUtil;

public class EditiqueService {
	
	
	public static byte[] printBdocOnDemand(File flux) throws FileNotFoundException, JSONException {
		
		
		String fluxEncoded = FileUtil.encodeFileToBase64(flux);
		
		String urlBdocOnDemand = ApplicationConfig.getUrlBdocOnDemand();
		
		JSONObject request = new JSONObject();
		request.put("correlationId", "TEST CIMR");
		request.put("templateName", "CIMR_CSE");
		request.put("isDatastreamReadFromFile", false);
		request.put("documentFormat", "PDF");
		request.put("assembleIf", false);
		request.put("datastreamContentAsBase64", fluxEncoded);
		request.put("customDocFlowName", "");
		request.put("keepDocumentsInSpool", false);
		request.put("timeout", 0);
		request.put("priority", 0);
		
		JSONObject documentIdJson = post(request, urlBdocOnDemand);
		JSONObject documents = documentIdJson.getJSONArray("documents").getJSONObject(0);
		String id = documents.getString("id");
		
		String urlDocument = ApplicationConfig.getUrlBdocDocumentContent() + id + "/content";
		
		JSONObject documentContentJson = get(urlDocument);
		String content = documentContentJson.getString("content");
		byte[] document = FileUtil.decodeFileToBase64(content);
		
		return document;
	}

	public static JSONObject post(JSONObject request, String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> apiResponse = restTemplate.exchange(urlString, HttpMethod.POST, entity, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.CREATED) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
	
	public static JSONObject get(String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<String> apiResponse = restTemplate.getForEntity(urlString, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.CREATED) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
}
