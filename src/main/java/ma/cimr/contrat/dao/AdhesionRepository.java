package ma.cimr.contrat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Adhesion;

@Repository
public interface AdhesionRepository extends JpaRepository<Adhesion, Long>, JpaSpecificationExecutor<Adhesion>{
	
	@Query("select a from Adhesion a where a.contrat.statut in :statuts")
	List<Adhesion> findAllByStatut(List<Integer> statuts);
}
