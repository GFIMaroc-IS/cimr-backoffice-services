package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.RefProduit;

@Repository
public interface RefProduitRepository extends JpaRepository<RefProduit, Long>{

	public RefProduit findByCode( String code);
}
	