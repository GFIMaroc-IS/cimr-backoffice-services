package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Delegataire;

@Repository
public interface DelegataireRepository extends JpaRepository<Delegataire, Long>, JpaSpecificationExecutor<Delegataire>{
	
}
