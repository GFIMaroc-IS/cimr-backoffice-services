package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Utilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>{
	
	@Query("select u from Utilisateur u where u.username = :username")
	Utilisateur getUtilisateur(String username);
	
	@Query("select u from Utilisateur u where u.username = :username and u.password = :password")
	Utilisateur getUtilisateur(String username, String password);
}
