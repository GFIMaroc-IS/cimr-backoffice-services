package ma.cimr.contrat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Profil;

@Repository
public interface ProfilRepository extends JpaRepository<Profil, Long>{
	
	@Query("select p from Profil p, ProfilUtilisateur pu, Utilisateur u "
			+ "where pu.idProfil = p.id and u.id = pu.idUtilisateur and u.username = :username")
	List<Profil> getProfilsByUsername(String username);
}
