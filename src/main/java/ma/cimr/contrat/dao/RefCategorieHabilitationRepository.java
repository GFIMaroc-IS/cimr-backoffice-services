package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.RefCategorieHabilitation;

@Repository
public interface RefCategorieHabilitationRepository extends JpaRepository<RefCategorieHabilitation, Long>{

}
	